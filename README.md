![alt text](https://i.postimg.cc/pLxGrLXP/519ccabf79986f3718b7d03d4b38ea1d.jpg)

## ADIN
![alt text](https://i.postimg.cc/YSYkx2GK/Logo.png)

## AI Image Classifier Convolutional NN

Image Classification using Neural Network Algorithms; Specifically Convolutional neural network which dedicated for processing images and with Pytorch tensor image decomposition; Image will be converted into image-tensor reshaping.


## Image Classification

Each image will be reshaped into tensor; 
with:
```
    transforms.Compose([
            transforms.Resize(L, W),
            transforms.RandomRotation(30),
            transforms.RandomCrop(n)
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=mean_values, std=std_vals)
        ])
```
transforms: is a part of pytorch module that used mainly to expose images to multiple transformations; that will incremently advance 
              image classification model deduction accuracy; by continuously altering image visualisations; that will be reflected 
              to how the model transform each image to the corresponding tensor.
              For each corresponding tensor to the alernating image scalars; will determine precisely which image relating to the       
              corresponding tensor even image has different scalars than the input (Training-set) images.

             
 ▌ Those scalars are for example [
1. **Resizing image:** To get all the input image to the same size which facilitate processing images.
2. **RandomRotation:** To rotate image randomly rotating images with the specified 30° value.
3. **RandomCrop:** Randomly cropping images will also enhance model optimisation; by determining a value of (n), Each image will be    
                   re-modifying input images.
4. **RandomHorizontalFlip:** ![alt text](https://i.postimg.cc/cCwjmdpc/flower.png)
                              Flipping input images horizontally will initialise different scalars of the image visualisations
                              That engages significantly on **_model accuracy_**.
5. **ToTensor:** After these transformations on images; they have to be converted into tensors to get processed in the model.
                 Tensor is the main brick in the model processing.
6. **Normalize:** Eventually; Determining the mean of the rate of images actualisation and comparison process changes; is considered as the 
                  most important point in the classification process; as it will iteratively comparing between the deducted tensor of the 
                  processed image and actual image; determining the mean and the standard deviation will be used to reduce the difference
                  between the deducted value and the actual value as stated before.
]   
<details><summary>°Note that:</summary>
╚ This process of transformation will be iterated through *Training | Testing* image sets.
</details>

▌Loading and sequencing datasets into equal batches.
```
    datasets.ImageFolder(train_dir, transform=trn_img_trnsfrms)
    torch.utils.data.DataLoader(trn_img_dataset, batch_size = 64, shuffle = True)
```

▌Then importing **_JSON_** is the next step for labeling each deducted image with its corresponding name.
```
import json

with open('cat_to_name.json', 'r') as f:
    cat_to_name = json.load(f)
```
![alt text](https://i.postimg.cc/V5PYy8Bx/Screenshot-2022-07-12-at-10-33-38-Create-Your-Own-Image-Classifier.png)

# Model Training

```
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
```
Initially while processing images; It's most advisable to use gpu image processing resposible cores **_CUDA Cores_** which 
might save alot of time if compared with normal cpu processing cores.

```
models.vgg16(pretrained = True)
```
▌_**vgg16**_ is a type of convolutional deep neural network architectures for classifying images into different classes
             __Using Keras module.
<details><summary>•Differnece between Imagenet classification dataset</summary>
     VggNet | ResNet | AlexNet | Inception and Xception are types of 
     ready-to-use deep Convolutional Neural networks models  نماذج الشبكة العصبية المتصلة العميقة ;
     That equipped with adjusted numbers of input nodes which might be given from:
        transforms.resize(224, 224) >> 
        °Note that: is the standard image size for model image manipulations.

$`input_n = (224\times 224)/2`$

Which equals (25088)° considered as number of nodes to the pre-trained model; and classifying images into 1000 different objects.
</details>

<details><summary>•VGG</summary>
Stands for Visual Geometry group and divided into 2 main types vgg16/vgg19; differs on number of convolutional layers.
vgg16 uses 16 layers and vgg19 uses 19 layers; with 92.7% of accuracy.
![alt text](https://viso.ai/wp-content/uploads/2021/10/vgg-neural-network-architecture.png)

vgg16 is pre-trained model that had been trained to classify multiple image types; with a consistent structure of 
   13 convolutional layers and 3 fully connected layers.
</details>
